package com.epam.restcontroller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDTO;
import com.epam.service.Services;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("rd/associates")
@RequiredArgsConstructor
public class Controller {

	private final Services service;

	@PostMapping
	public ResponseEntity<AssociateDTO> create(@RequestBody @Valid AssociateDTO associateDTO) {
		return new ResponseEntity<>(service.add(associateDTO), HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable int id) {
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping
	public ResponseEntity<AssociateDTO> update(@RequestBody @Valid AssociateDTO associateDTO) {
		return new ResponseEntity<>(service.update(associateDTO), HttpStatus.OK);
	}

	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDTO>> getByGender(@PathVariable String gender) {
		return new ResponseEntity<>(service.getByGender(gender), HttpStatus.OK);
	}

}
