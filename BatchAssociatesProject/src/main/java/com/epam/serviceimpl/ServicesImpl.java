package com.epam.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.dao.AssociateRepository;
import com.epam.dao.BatchRepository;
import com.epam.dto.AssociateDTO;
import com.epam.dto.BatchDTO;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.exception.AssociateException;
import com.epam.service.Services;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class ServicesImpl implements Services {

	private final BatchRepository batchRepository;
	private final AssociateRepository associatesRepository;
	private final ModelMapper mapper;

	@Override
	public AssociateDTO add(AssociateDTO associateDto) {

		Associate associate = dtoToAssociate(associateDto);

		Optional<Batch> tempBatch = batchRepository.findById(associate.getBatch().getId());

		Batch batch = associate.getBatch();

		if (tempBatch.isEmpty()) {
			batchRepository.save(dtoToBatch(associateDto.getBatch()));
		} else {
			batch = tempBatch.get();
		}

		associate.setBatch(batch);

		return associateToDto(associatesRepository.save(associate));
	}

	@Override
	public void delete(int id) {
		associatesRepository.deleteById(id);
	}

	@Override
	public AssociateDTO update(AssociateDTO associateDto) {

		if (!associatesRepository.existsById(associateDto.getId())) {
			throw new AssociateException("Associate not found");
		}

		Associate associate = dtoToAssociate(associateDto);

		Optional<Batch> tempBatch = batchRepository.findById(associate.getBatch().getId());

		Batch batch = associate.getBatch();

		if (tempBatch.isEmpty()) {
			batchRepository.save(dtoToBatch(associateDto.getBatch()));
		} else {
			batch = tempBatch.get();
		}

		associate.setBatch(batch);

		return associateToDto(associatesRepository.save(associate));
	}

	@Override
	public List<AssociateDTO> getByGender(String gender) {
		if (!(gender.equalsIgnoreCase("M") || gender.equalsIgnoreCase("F"))) {
			throw new AssociateException("Invalid Gender type");
		}
		return associatesRepository.findAllByGender(gender).stream().map(this::associateToDto).toList();
	}

	private AssociateDTO associateToDto(Associate associate) {
		return mapper.map(associate, AssociateDTO.class);
	}

	private Associate dtoToAssociate(AssociateDTO associateDto) {
		return mapper.map(associateDto, Associate.class);
	}

	private Batch dtoToBatch(BatchDTO batchDto) {
		return mapper.map(batchDto, Batch.class);
	}

}
