package com.epam.exceptionhandling;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.dto.ExceptionResponse;
import com.epam.exception.AssociateException;

@RestControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(AssociateException.class)
	public ResponseEntity<ExceptionResponse> AssociateExceptionHandling(AssociateException branchException, WebRequest request) {
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				branchException.getMessage(), request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> methodArgumentExceptionHandling(MethodArgumentNotValidException e,
			WebRequest request) {
		Map<String, String> result = new HashMap<>();
		e.getBindingResult().getFieldErrors().forEach(error -> result.put(error.getField(), error.getDefaultMessage()));
		String message = result.toString();
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), message,
				request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ExceptionResponse> exceptionHandling(RuntimeException runtimeException, WebRequest request) {
		String message = "Something Went Wrong : " + runtimeException.getMessage();
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),
				message, request.getDescription(false)), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ExceptionResponse> sqlExceptionHandling(
			DataIntegrityViolationException dataIntegrityViolationException, WebRequest request) {
		String message = "Batch name is already present";
		return new ResponseEntity<>(new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), message,
				request.getDescription(false)), HttpStatus.BAD_REQUEST);
	}

}
