package com.epam.service;

import java.util.List;

import com.epam.dto.AssociateDTO;

public interface Services {

	AssociateDTO add(AssociateDTO associateDto);

	void delete(int id);

	AssociateDTO update(AssociateDTO associateDto);

	List<AssociateDTO> getByGender(String gender);

}
