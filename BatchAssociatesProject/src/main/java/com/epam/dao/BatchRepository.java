package com.epam.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.entity.Batch;

@Repository
public interface BatchRepository extends JpaRepository<Batch, Integer> {

}
