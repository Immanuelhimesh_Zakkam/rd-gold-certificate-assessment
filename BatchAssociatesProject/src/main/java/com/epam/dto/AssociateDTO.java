package com.epam.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AssociateDTO {

	@NotNull(message = "Id entered is invalid")
	int id;

	@NotBlank(message = "Name can't be Empty")
	String name;

	@NotBlank(message = "Email can't be Empty")
	String email;

	@NotBlank(message = "Gender can't be Empty")
	@Size(max = 1)
	String gender;

	@NotBlank(message = "College can't be Empty")
	String college;

	@NotBlank(message = "Status can't be Empty")
	String status;

	BatchDTO batch;
}
