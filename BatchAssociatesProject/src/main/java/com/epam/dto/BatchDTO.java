package com.epam.dto;

import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BatchDTO {

	@Schema(accessMode = AccessMode.READ_ONLY)
	int id;

	@NotBlank(message = "Name can't be Empty")
	String name;

	@NotBlank(message = "Practice can't be Empty")
	String practice;

	Date startDate;

	Date endDate;
}
