package com.epam.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ExceptionResponse {
	
	private String timeStamp;
	private String status;
	private String exception;
	private String path;
	
}
