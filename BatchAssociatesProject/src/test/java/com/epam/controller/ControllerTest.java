package com.epam.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.dto.AssociateDTO;
import com.epam.dto.BatchDTO;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.exception.AssociateException;
import com.epam.restcontroller.Controller;
import com.epam.service.Services;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(Controller.class)
class ControllerTest {

	@MockBean
	private Services service;

	@Autowired
	private MockMvc mockMvc;

	private Associate associate;
	private AssociateDTO associateDTO;
	private Batch batch;
	private BatchDTO batchDTO;

	@BeforeEach
	void preTest() {
		batch = new Batch();
		batch.setId(1);
		batch.setName("1");
		batch.setPractice("1");
		associate = new Associate();
		associate.setCollege("1");
		associate.setEmail("1");
		associate.setGender("m");
		associate.setId(1);
		associate.setName("1");
		associate.setStatus("1");
		associate.setBatch(batch);
		batchDTO = new BatchDTO();
		batchDTO.setId(1);
		batchDTO.setName("1");
		batchDTO.setPractice("1");
		associateDTO = new AssociateDTO();
		associateDTO.setCollege("1");
		associateDTO.setEmail("1");
		associateDTO.setGender("m");
		associateDTO.setId(1);
		associateDTO.setName("1");
		associateDTO.setStatus("1");
		associateDTO.setBatch(batchDTO);
	}

	@Test
	void testAddAssociate() throws JsonProcessingException, Exception {

		Mockito.when(service.add(any(AssociateDTO.class))).thenReturn(associateDTO);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isCreated());

		Mockito.verify(service).add(any(AssociateDTO.class));

	}

	@Test
	void testAddAssociateWithDataIntegrityException() throws JsonProcessingException, Exception {

		Mockito.when(service.add(any(AssociateDTO.class))).thenThrow(DataIntegrityViolationException.class);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isBadRequest());

		Mockito.verify(service).add(any(AssociateDTO.class));

	}

	@Test
	void testAddAssociateWithRunTimeException() throws JsonProcessingException, Exception {

		Mockito.when(service.add(any(AssociateDTO.class))).thenThrow(RuntimeException.class);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO)))
				.andExpect(status().isInternalServerError());

		Mockito.verify(service).add(any(AssociateDTO.class));

	}

	@Test
	void testAddAssociateWithAssociatesException() throws JsonProcessingException, Exception {

		Mockito.when(service.add(any(AssociateDTO.class))).thenThrow(AssociateException.class);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isBadRequest());

		Mockito.verify(service).add(any(AssociateDTO.class));

	}

	@Test
	void testAddAssociateWithMethodArgumentNotValidException() throws JsonProcessingException, Exception {

		AssociateDTO temp = new AssociateDTO();

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(temp))).andExpect(status().isBadRequest());

	}

	@Test
	void testUpdateAssociate() throws JsonProcessingException, Exception {

		Mockito.when(service.update(any(AssociateDTO.class))).thenReturn(associateDTO);

		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isOk());

		Mockito.verify(service).update(any(AssociateDTO.class));

	}

	@Test
	void testDeleteAssociate() throws Exception {
		Mockito.doNothing().when(service).delete(1);
		mockMvc.perform(delete("/rd/associates/{id}", 1)).andExpect(status().isNoContent());
		Mockito.verify(service).delete(1);
	}

	@Test
	void testGetByGender() throws Exception {
		List<AssociateDTO> list = new ArrayList<>();
		list.add(associateDTO);
		Mockito.when(service.getByGender(any(String.class))).thenReturn(list);

		mockMvc.perform(get("/rd/associates/{gender}", "m")).andExpect(status().isOk());

		Mockito.verify(service).getByGender(any(String.class));
	}
}
