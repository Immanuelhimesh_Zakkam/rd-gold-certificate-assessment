package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dao.AssociateRepository;
import com.epam.dao.BatchRepository;
import com.epam.dto.AssociateDTO;
import com.epam.dto.BatchDTO;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.exception.AssociateException;
import com.epam.serviceimpl.ServicesImpl;

@ExtendWith(MockitoExtension.class)
class ServiceTest {

	private Batch batch;
	private Associate associate;
	private BatchDTO batchDTO;
	private AssociateDTO associateDTO;

	@Mock
	private ModelMapper mapper;

	@Mock
	private BatchRepository batchRepository;

	@Mock
	private AssociateRepository associateRepository;

	@InjectMocks
	private ServicesImpl service;

	@BeforeEach
	void preTest() {
		batch = new Batch();
		batch.setId(1);
		batch.setName("1");
		batch.setPractice("1");
		associate = new Associate();
		associate.setCollege("1");
		associate.setEmail("1");
		associate.setGender("m");
		associate.setId(1);
		associate.setName("1");
		associate.setStatus("1");
		associate.setBatch(batch);
		batchDTO = new BatchDTO();
		batchDTO.setId(batch.getId());
		batchDTO.setName(batch.getName());
		batchDTO.setPractice(batch.getPractice());
		associateDTO = new AssociateDTO();
		associateDTO.setCollege(associate.getCollege());
		associateDTO.setEmail(associate.getEmail());
		associateDTO.setGender(associate.getGender());
		associateDTO.setId(associate.getId());
		associateDTO.setName(associate.getName());
		associateDTO.setStatus(associate.getStatus());
		associateDTO.setBatch(batchDTO);
	}

	@Test
	void testAdd() {
		Mockito.when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(mapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.findById(associate.getBatch().getId())).thenReturn(Optional.of(batch));
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);

		AssociateDTO result = service.add(associateDTO);
		assertEquals(associateDTO, result);
	}

	@Test
	void testAddWithwrongDetails() {
		Mockito.when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(mapper.map(batchDTO, Batch.class)).thenReturn(batch);
		Mockito.when(mapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.findById(batch.getId())).thenReturn(Optional.empty());
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);

		assertEquals(associateDTO, service.add(associateDTO));
		
	}

	@Test
	void testUpdateWithException() {

		batch.setStartDate(Date.from(Instant.now()));
		batch.setEndDate(batch.getStartDate());
		
		assertThrows(AssociateException.class, () -> service.update(associateDTO));
	}
	
	@Test
	void testUpdate() {
		Mockito.when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(mapper.map(batchDTO, Batch.class)).thenReturn(batch);
		Mockito.when(mapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.findById(batch.getId())).thenReturn(Optional.empty());
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		Mockito.when(associateRepository.existsById(associateDTO.getId())).thenReturn(true);

		assertEquals(associateDTO, service.update(associateDTO));
	}
	
	@Test
	void testUpdateWithWrongDetails() {
		Mockito.when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(mapper.map(associateDTO, Associate.class)).thenReturn(associate);
		Mockito.when(batchRepository.findById(batch.getId())).thenReturn(Optional.of(batch));
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(associateRepository.existsById(associateDTO.getId())).thenReturn(true);

		assertEquals(associateDTO, service.update(associateDTO));
	}

	@Test
	void testDelete() {
		Mockito.doNothing().when(associateRepository).deleteById(1);
		service.delete(1);
		Mockito.verify(associateRepository).deleteById(1);
	}
	
	@Test
	void testGetByGenderWithException() {
		List<Associate> list=new ArrayList<>();
		list.add(associate);
		assertThrows(AssociateException.class,() -> service.getByGender("o"));
	}
	
	@Test
	void testGetByGenderWithException2() {
		List<Associate> list=new ArrayList<>();
		list.add(associate);
		batch.setEndDate(Date.from(Instant.now()));
		batch.setStartDate(batch.getEndDate());
		assertThrows(AssociateException.class,() -> service.getByGender("k"));
	}
	
	@Test
	void testGetByGenderWithM() {
		associate.setGender("M");
		associateDTO.setGender("M");
		List<AssociateDTO> list=new ArrayList<>();
		list.add(associateDTO);
		List<Associate> list1=new ArrayList<>();
		list1.add(associate);
		Mockito.when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(associateRepository.findAllByGender("M")).thenReturn(list1);
		assertEquals(list, service.getByGender("M"));
	}
	
	@Test
	void testGetByGenderWithF() {
		associate.setGender("F");
		associateDTO.setGender("F");
		List<AssociateDTO> list=new ArrayList<>();
		list.add(associateDTO);
		List<Associate> list1=new ArrayList<>();
		list1.add(associate);
		Mockito.when(mapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		Mockito.when(associateRepository.findAllByGender("F")).thenReturn(list1);
		assertEquals(list, service.getByGender("F"));
	}
}
